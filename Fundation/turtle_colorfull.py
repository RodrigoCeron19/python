import turtle

tortuga = turtle.Turtle()
colors = ['red','purple','blue','green','yellow','orange']
for x in range(360):
    tortuga.pencolor(colors[x % 6])
    tortuga.width(x/100+1)
    tortuga.forward(x)
    tortuga.left(59)